import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CustomValidators } from './custom-validators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public formSignup: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.formSignup = this.createSignupForm();
  }

  createSignupForm(): FormGroup {
    return this.formBuilder.group(
      {
        username: [
          null,
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            CustomValidators.patternValidator(/[^a-zA-Z0-9]/, {
              hasLetterAndNumber: true
            })
          ])
        ],
        email: [
          null,
          Validators.compose([Validators.email, Validators.required])
        ],
        password: [
          null,
          Validators.compose([
            Validators.required,
            CustomValidators.patternValidator(/\d/, {
              hasNumber: true
            }),
            CustomValidators.patternValidator(/[A-Z]/, {
              hasUpperCase: true
            }),
            CustomValidators.patternValidator(/[a-z]/, {
              hasLowerCase: true
            }),
            CustomValidators.patternValidator(
              /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
              hasSpecialCharacters: true
            }),
            Validators.minLength(8),
            Validators.maxLength(100)
          ])
        ],
        confirmPassword: [null, Validators.compose([Validators.required])]
      },
      {
        validator: CustomValidators.passwordMatchValidator
      }
    );
  }

  submit() {
    console.log(this.formSignup.value);
  }
}
